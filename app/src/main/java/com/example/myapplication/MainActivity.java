package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, Frguno.OnFragmentInteractionListener, Frgdos.OnFragmentInteractionListener {
    Button btnFRGuno, btnFRGdos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnFRGuno = (Button) findViewById(R.id.btnfrguno);
        btnFRGdos = (Button) findViewById(R.id.btnfrgdos);
        btnFRGuno.setOnClickListener(this);
        btnFRGdos.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case  R.id.btnfrguno:
            Frguno fragmentouno = new Frguno();
                FragmentTransaction transactionUno = getSupportFragmentManager().beginTransaction();
                transactionUno.replace(R.id.contenedor,fragmentouno);
                transactionUno.commit();
                break;
            case R.id.btnfrgdos:
                Frgdos fragmentodos = new Frgdos();
                FragmentTransaction transactionDos = getSupportFragmentManager().beginTransaction();
                transactionDos.replace(R.id.contenedor, fragmentodos);
                transactionDos.commit();
                break;
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
